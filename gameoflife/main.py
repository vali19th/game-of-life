# This file is part of the Game of Life program
# Copyright (C) 2017 Valentin Neagu <vali19th@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this program; see the file LICENSE. If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.

import sys
import threading
from random import randint

from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtWidgets import QApplication, QGridLayout, QHBoxLayout, QPushButton, QVBoxLayout, QWidget, QLabel


class World(QWidget):
    def __init__(self):
        super(World, self).__init__()
        self.x_max = 25
        self.y_max = 25

        layout = QGridLayout()
        layout.setSpacing(0)
        self.cells = self.create_world()

        for x in range(self.x_max):
            for y in range(self.y_max):
                self.cells[x][y].setCheckable(True)
                layout.addWidget(self.cells[x][y], x, y)
                self.cells[x][y].setStyleSheet('width: 15px; height: 15px')

        self.setLayout(layout)

    def create_world(self):
        return [[QPushButton()
            for y in range(self.y_max)]
            for x in range(self.x_max)]

    def count_neighbours(self, x, y, world):
        neighbours_alive = 0

        for cell_x in (x - 1, x, x + 1):
            for cell_y in (y - 1, y, y + 1):
                if 0 <= cell_x < self.x_max and 0 <= cell_y < self.y_max:
                    if self.cells[cell_x][cell_y].isChecked():
                            neighbours_alive += 1

        # the targeted cell doesn't count
        if self.cells[x][y].isChecked():
            neighbours_alive -= 1

        return neighbours_alive

    def natural_selection(self, x, y, world):
        neigbours = self.count_neighbours(x, y, world)

        if neigbours == 3 or (neigbours == 2 and self.cells[x][y].isChecked()):
            return 1
        else:
            return 0

    def update_world(self, world, seed='next'):
        next_cells = [[0 for x in range(self.x_max)] for y in range(self.y_max)]

        if seed == 'next':
            for x in range(self.x_max):
                for y in range(self.y_max):
                    next_cells[x][y] = self.natural_selection(x, y, world)

        elif seed == 'random':
            for x in range(self.x_max):
                for y in range(self.y_max):
                    next_cells[x][y] = randint(0, 1)

        for x in range(self.x_max):
            for y in range(self.y_max):
                if(next_cells[x][y]):
                    self.cells[x][y].setChecked(True)
                else:
                    self.cells[x][y].setChecked(False)


class GUI(QWidget):
    def __init__(self):
        super(GUI, self).__init__()
        self.setFont(QFont('', 16))
        self.setWindowTitle('Conway\'s Game of Life')
        self.setWindowIcon(QIcon('assets/python.png'))
        self.setStyleSheet('QPushButton:checked, QPushButton:pressed  { background-color: #7FC18E}')

        self.play_btn = QPushButton('Play')
        self.play_btn.released.connect(self.play)
        self.play_btn.setShortcut('Space')
        self.play_btn.setCheckable(True)

        next_btn = QPushButton('Next')
        next_btn.clicked.connect(self.next)
        next_btn.setShortcut('N')

        generate_btn = QPushButton('Randomize')
        generate_btn.clicked.connect(self.randomize)
        generate_btn.setShortcut('R')

        reset_btn = QPushButton('Destroy')
        reset_btn.clicked.connect(self.destroy)
        reset_btn.setShortcut('D')

        quit_btn = QPushButton('Quit')
        quit_btn.clicked.connect(sys.exit)
        quit_btn.setShortcut('Q')

        menu_layout = QVBoxLayout()
        menu_layout.addStretch()
        menu_layout.addWidget(self.play_btn)
        menu_layout.addWidget(next_btn)
        menu_layout.addWidget(generate_btn)
        menu_layout.addWidget(reset_btn)
        menu_layout.addWidget(QLabel('\n'.join(['\n' * 3,
            'Shortcuts: \n',
            'P - play/pause',
            'N - next',
            'R - randomize',
            'D - destroy',
            'Q - quit',
            '\n' * 3])))
        menu_layout.addWidget(quit_btn)
        menu_layout.addStretch()

        self.world = World()
        menu = QWidget()
        menu.setLayout(menu_layout)

        layout = QHBoxLayout()
        layout.addWidget(self.world)
        layout.addWidget(menu)
        self.setLayout(layout)

    def play(self, dt=0.3):
        if self.play_btn.isChecked():
            self.play_btn.setText('Playing')
            self.world.setEnabled(False)
            t = threading.Timer(dt, self.repeat_next)
            t.start()
        else:
            self.play_btn.setText('Play')
            self.world.setEnabled(True)

    def repeat_next(self, dt=0.3):
        if self.play_btn.isChecked():
            self.next()
            t = threading.Timer(dt, self.repeat_next)
            t.start()

    def next(self):
        self.world.update_world(self.world)

    def randomize(self):
        self.world.update_world(self.world, 'random')

    def destroy(self):
        self.world.update_world(self.world, '')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    gui = GUI()
    gui.show()

    sys.exit(app.exec_())
