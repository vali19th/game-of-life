# This file is part of the Game of Life program
# Copyright (C) 2017 Valentin Neagu <vali19th@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this program; see the file LICENSE. If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Conway\'s game of life implemented using python and Qt5',
    'author': 'Valentin Neagu',
    'url': '###',
    'download_url': '###',
    'author_email': 'vali19th@gmail.com',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['gameoflife'],
    'scripts': [],
    'name': 'gameoflife'}

setup(**config)
